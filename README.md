# GitLab 13.12 Auto DevOps and Secure CI templates

This is an extraction of CI templates as of GitLab 13.12, prior to
various breaking changes that were made in GitLab 14.0.

## How to use

You can use these templates in a few ways

- Via `include:remote`
- Setting up a new project and referencing via `include:local`

### Include using `include:remote`

1. Update your `.gitlab-ci.yml` file to refer to the 13.12 template that is hosted on GitLab.com

    ```yaml
    include:
      - remote: 'https://gitlab.com/hfyngvason/auto-devops-v13-12/-/raw/master/Auto-DevOps-remote.gitlab-ci.yml'
    ```

### Include using a new project, and using `include:local`

An instance-local alternative to `include:remote`. In this case, the
templates are retrieved from a local project on your GitLab instance.

1. Create a new project that for templates from this project. On your GitLab instance, go to New Project >> Import Project.
   Select Repo by URL option, and enter
   `https://gitlab.com/hfyngvason/auto-devops-v13-12.git` as the Git repository URL.
1. Update your `.gitlab-ci.yml` file to refer to the 13.12 template.

    ```yaml
    include:
      - project: 'my-group/my-project'
        file: 'Auto-DevOps.gitlab-ci.yml'
    ```
